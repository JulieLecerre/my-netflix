<?php
class Constants {
    public static $firstNameErrors = "Le prénom doit se composer de 2 à 25 caractères";
    public static $lastNameErrors = "Le nom doit se composer de 2 à 25 caractères";
    public static $usernameErrors = "Votre pseudo doit se composer de 2 à 25 caractères";
    public static $usernameTaken = "Pseudo déjà utilisé";
    public static $emailsErrors = "Emails différents";
    public static $emailInvalid = "Email invalide";
    public static $emailTaken = "Cet email est déjà associé à un compte";
    public static $passwordsErrors = "Mots de passe différents";
    public static $passwordLength = "Le mot de passe doit se composer de 5 à 25 caractères";
    public static $loginFailed = "Votre mot de passe ou votre adresse mail sont incorrects";
}
?>
