<?php

class Entity {

    private $con, $sqlData;

    public function __construct($con, $input) {
        $this->con = $con;

        if(is_array($input)){
            $this->sqlData = $input;
        }
        else {
            $query = $this->con->prepare("SELECT * FROM entities WHERE id=:id");
            $query->bindValue(":id", $input);
            $query->execute();

            $this->sqlData = $query->fetch(PDO::FETCH_ASSOC);
        }
    }
    
    public function getId() {
        return $this->sqlData["id"];
    }
    public function getTitle() {
        return $this->sqlData["title"];
    }
    public function getDescription() {
        return $this->sqlData["description"];
    }
    public function getImage() {
        return $this->sqlData["image"];
    }
    public function getDate_out() {
        return $this->sqlData["date_out"];
    }
    public function getEvaluation() {
        return $this->sqlData["evaluation"];
    }
    public function getActors() {
        return $this->sqlData["actors"];
    }
    public function getVideo() {
        return $this->sqlData["video"];
    }
}

?>