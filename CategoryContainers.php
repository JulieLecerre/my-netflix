<?php

class CategoryContainers {

    private $con, $username;

    public function __construct($con, $username) {
        $this->con = $con;
        $this->username = $username;
    }

    public function showAllCategories() {
        $query = $this->con->prepare("SELECT * FROM categories");
        $query->execute();
        
        $html = "<div class='previewCategories'>";

        while($row = $query->fetch(PDO::FETCH_ASSOC)) {
            $html .= $this->getCategoryHtml($row, null);
        }

        return $html . "</div>";
    }

    private function getCategoryHtml($sqlData, $title) {
        $id_category = $sqlData["id"];
        $movies = $this->getMoviesFromCategoryId($id_category);
        $title = $title == null ? $sqlData["name"] : $title;

        $result = "<b>".$title."</b><br>";
        while($row = $movies->fetch(PDO::FETCH_ASSOC)) {
            $result = $result . $row["title"] . "<br>";
        }
        $result = $result . "<br>";

        return $result;
    }

    private function getMoviesFromCategoryId($id_category) {
        $query = $this->con->prepare("SELECT * FROM entities WHERE id_category = :id_category");
        $query->bindValue(":id_category", $id_category);

        $query->execute();

        return $query;
    }

}
?>