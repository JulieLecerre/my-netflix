<?php

class PreviewProvider {

    private $con, $username;

    public function __construct($con, $username) {
        $this->con = $con;
        $this->username = $username;
    }

    public function createPreviewVideo($entity) {
        if($entity == null) {
            $entity = $this->getRandomEntity();
        }
        $id = $entity->getId();
        $title = $entity->getTitle();
        $description = $entity->getDescription();
        $image = $entity->getImage();
        $date_out = $entity->getDate_out();
        $evaluation = $entity->getEvaluation();
        $actors = $entity->getActors();
        $video = $entity->getVideo();

        return "<div class='previewContainer'>
                    <img src='$image' class='previewImage' hidden>

                    <video autoplay muted class='previewVideo'>
                        <source src='$video' type='video/mp4'>
                    </video>

                    <div class='previewOverlay'>
                        <div class='mainDetails'>
                            <h3>$title</h3>

                            <div class='buttons'>
                                <button>Voir la fiche du film</button>
                        </div>
                    </div>

                </div>";
    }

    private function getRandomEntity() {

        $query = $this->con->prepare("SELECT * FROM entities ORDER BY RAND() LIMIT 1");
        $query->execute();

        $row = $query->fetch(PDO::FETCH_ASSOC);
        
        return new Entity($this->con, $row);
    }
}

?>