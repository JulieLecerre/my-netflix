<?php
require_once("config.php");
require_once("VerificationForm.php");
require_once("Constants.php");
require_once("Account.php");

$account = new Account($con);

    if(isset($_POST["submitButton"])) {

        $username = VerificationForm::VerificationFormUsername($_POST["username"]);
        $password = VerificationForm::VerificationFormPassword($_POST["password"]);
        
        $success = $account->login($username, $password);

        if($success) {
            $_SESSION["userLoggedIn"] = $username;
            header("Location: index.php");
        }
    }

function getInputValue($name) {
    if(isset($_POST[$name])) {
        echo $_POST[$name];
    }
}  
?>
<!DOCTYPE html>
<html>
    <head>
        <title>Bienvenue !</title>
        <link rel="stylesheet" type="text/css" href="style.css" />
    </head>
    <body>
        
        <div class="signInContainer">

            <div class="column">

                <div class="header">
                    <h3>Se connecter</h3>
                </div>

                <form method="POST">
                    <?php echo $account->getError(Constants::$loginFailed); ?>
                    <input type="text" name="username" placeholder="Pseudo" value="<?php getInputValue("username"); ?>" required>

                    <input type="password" name="password" placeholder="Mot de passe" required>

                    <input type="submit" name="submitButton" value="Envoyer">

                </form>

            </div>

        </div>

    </body>
</html>