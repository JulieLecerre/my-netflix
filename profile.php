<?php

require_once("header.php");


?>

<div class="settingsContainer column">

    <div class="formSection">

        <form method="POST">

            <h2>Profile utilisateur<h2>
                <?php
                $user = new User($con, $userLoggedIn);

                $firstName = isset($_POST["firstName"]) ? ($_POST["firstName"]) : $user->getFirstName();
                $lastName = isset($_POST["lastName"]) ? ($_POST["lastName"]) : $user->getLastName();
                $email = isset($_POST["email"]) ? ($_POST["email"]) : $user->getEmail();
                ?>

            <input type="text" name="firstName" placeholder="Prénom" value="<?php echo $firstName; ?>">
            <input type="text" name="lastName" placeholder="Nom" value="<?php echo $lastName; ?>">
            <input type="text" name="email" placeholder="Email" value="<?php echo $email; ?>">

            <input type="submit" name="saveDetailsButton" value="Enregistrer">
        </form>
    </div>

    <div class="formSection">

        <form method="POST">

            <h2>Modifier le mot de passe<h2>

            <input type="password" name="oldPassword" placeholder="Ancien mot de passe">
            <input type="password" name="newPassword" placeholder="Nouveau mot de passe">
            <input type="password" name="newPassword2" placeholder="Confirmer le nouveau mot de passe">

            <input type="submit" name="savePasswordButton" value="Enregistrer">
        </form>
    </div>
</div>