<?php
ob_start();
session_start();

date_default_timezone_set("Europe/Paris");                                      //fuseau horaire

try {
    $con = new PDO("mysql:dbname=netflix;host=localhost", "root", "");       //connecion à mysql : name de la bdd, le localhost, le username by default, le password vide)
    $con->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_WARNING);                                   //Le mode pour reporter les erreurs de PDO. et pour émettre des diagnostics
}

catch (PDOException $e) {
  exit ("Connection failed: " . $e->getMessage());                          //s'il y a une erreur dans la connection avec la bdd
}
?>
