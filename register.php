<?php
require_once("config.php");
require_once("VerificationForm.php");
require_once("Constants.php");
require_once("Account.php");

    $account = new Account($con);

    if(isset($_POST["submitButton"])) {
        
        $firstName = VerificationForm::VerificationFormString($_POST["firstName"]);
        $lastName = VerificationForm::VerificationFormString($_POST["lastName"]);
        $username = VerificationForm::VerificationFormUsername($_POST["username"]);
        $email = VerificationForm::VerificationFormEmail($_POST["email"]);
        $email2 = VerificationForm::VerificationFormEmail($_POST["email2"]);
        $password = VerificationForm::VerificationFormPassword($_POST["password"]);
        $password2 = VerificationForm::VerificationFormPassword($_POST["password2"]);
        
        $success = $account->register($firstName, $lastName, $username, $email, $email2, $password, $password2);

        if($success) {
            $_SESSION["userLoggedIn"] = $username;
            header("Location: index.php");
        }
    }

function getInputValue($name) {
    if(isset($_POST[$name])) {
        echo $_POST[$name];
    }
}  
?>
<!DOCTYPE html>
<html>
    <head>
        <title>Bienvenue !</title>
        <link rel="stylesheet" type="text/css" href="style.css" />
    </head>
    <body>
        
        <div class="signInContainer">

            <div class="column">

                <div class="header">
                    <h3>Formulaire d'inscription</h3>
                </div>

                <form method="POST">

                    <?php echo $account->getError(Constants::$firstNameErrors); ?>
                    <input type="text" name="firstName" placeholder="Prénom" value="<?php getInputValue("firstName"); ?>" required>

                    <?php echo $account->getError(Constants::$lastNameErrors); ?>
                    <input type="text" name="lastName" placeholder="Nom" value="<?php getInputValue("lastName"); ?>" required>
                    
                    <?php echo $account->getError(Constants::$usernameErrors); ?>
                    <?php echo $account->getError(Constants::$usernameTaken); ?>
                    <input type="text" name="username" placeholder="Pseudo" value="<?php getInputValue("username"); ?>" required>

                    <?php echo $account->getError(Constants::$emailsErrors); ?>
                    <?php echo $account->getError(Constants::$emailInvalid); ?>
                    <?php echo $account->getError(Constants::$emailTaken); ?>
                    <input type="email" name="email" placeholder="Email" value="<?php getInputValue("email"); ?>" required>

                    <input type="email" name="email2" placeholder="Confirmer email" value="<?php getInputValue("email2"); ?>" required>
                    
                    <?php echo $account->getError(Constants::$passwordsErrors); ?>
                    <?php echo $account->getError(Constants::$passwordLength); ?>
                    <input type="password" name="password" placeholder="Mot de passe" required>

                    <input type="password" name="password2" placeholder="Confirmer mot de passe" required>
                    <legend>Abonnements</legend>
                    <label><input type="radio" name="radio">Offre engagement mensuel 10€/mois</label>
                    <label><input type="radio" name="radio">Offre engagement annuel 100€/an</label>

                    <input type="submit" name="submitButton" value="Envoyer">

                </form>


            </div>

        </div>

    </body>
</html>