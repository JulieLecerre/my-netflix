<?php

require_once("config.php");
require_once("PreviewProvider.php");
require_once("Entity.php");
require_once("CategoryContainers.php");
require_once("User.php");

if(!isset($_SESSION["userLoggedIn"])) {
    header("Location: register.php");
}

$userLoggedIn = $_SESSION["userLoggedIn"];

?>

<!DOCTYPE html>
<html>
    <head>
        <title>Bienvenue !</title>
        <link rel="stylesheet" type="text/css" href="style.css" />
    </head>
    <body>
    <div class="wrapper">